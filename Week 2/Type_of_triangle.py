def cos_a(a, b, c):
    return (a ** 2 + b ** 2 - c ** 2) / (2 * a * b)


x1 = int(input())
x2 = int(input())
x3 = int(input())

if cos_a(x1, x2, x3) == 0 or cos_a(x2, x3, x1) == 0 or cos_a(x3, x1, x2) == 0:
    print('rectangular')
elif -1 < cos_a(x1, x2, x3) < 0 or -1 < cos_a(x2, x3, x1) < 0 or \
        -1 < cos_a(x3, x1, x2) < 0:
    print('obtuse')
elif cos_a(x1, x2, x3) > 0 and cos_a(x2, x3, x1) > 0 and cos_a(x3, x1, x2) > 0:
    print('acute')
else:
    print('impossible')
