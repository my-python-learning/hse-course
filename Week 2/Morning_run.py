X = int(input())
Y = int(input())

day = 1
increase = 0.1
while X < Y:
    day += 1
    X = X + X * increase
else:
    print(day)
