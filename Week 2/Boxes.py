def is_one_in(x, y):
    if x[0] == y[0] and x[1] == y[1] and x[2] == y[2]:
        print('Boxes are equal')
    elif x[0] <= y[0] and x[1] <= y[1] and x[2] <= y[2]:
        print('The first box is smaller than the second one')
    elif x[0] >= y[0] and x[1] >= y[1] and x[2] >= y[2]:
        print('The first box is larger than the second one')
    else:
        print('Boxes are incomparable')


def sort_nums(a, b, c):
    if a > c:
        a, c = c, a

    if a > b:
        a, b = b, a

    if b > c:
        b, c = c, b
    return a, b, c


A1, B1, C1 = int(input()), int(input()), int(input())
A2, B2, C2 = int(input()), int(input()), int(input())

A1, B1, C1 = sort_nums(A1, B1, C1)
A2, B2, C2 = sort_nums(A2, B2, C2)

is_one_in([A1, B1, C1], [A2, B2, C2])
