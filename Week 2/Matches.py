l1, r1 = int(input()), int(input())
l2, r2 = int(input()), int(input())
l3, r3 = int(input()), int(input())

if l1 <= l2 <= l3:
    if (r2 < l3 and r2 + (r1 - l1) >= l3) or (r2 >= l3 and r1 < l2):
        print(1)
    elif (r1 < l2 and r1 + (r3 - l3) >= l2) or (r1 >= l2 and r2 < l3):
        print(3)
    elif (r2 < l3 and r2 + (r1 - l1) < l3) or (r1 < l2 and r1 + (l3 - r3) < l2):
        print(-1)
    elif r1 >= l2 and r2 >= l3:
        print(0)
elif l1 <= l3 <= l2:
    if (r3 < l2 and r3 + (r1 - l1) >= l2) or (r3 >= l2 and r1 < l3):
        print(1)
    elif (r1 < l3 and r1 + (r2 - l2) >= l3) or (r1 >= l3 and r3 < l2):
        print(2)
    elif (r3 < l2 and r3 + (r1 - l1) < l2) or (r1 < l3 and r1 + (r2 - l2) < l3):
        print(-1)
    elif r1 >= l3 and r3 >= l2:
        print(0)
elif l2 <= l1 <= l3:
    if (r1 < l3 and r1 + (r2 - l2) >= l3) or (r1 >= l3 and r2 < l1):
        print(2)
    elif (r2 < l1 and r2 + (r3 - l3) >= l1) or (r2 >= l3 and r1 < l2):
        print(3)
    elif (r1 < l3 and r1 + (r2 - l2) < l3) or (r2 < l1 and r1 + (r3 - l3) < l1):
        print(-1)
    elif r2 >= l1 and r1 >= l3:
        print(0)
elif l2 <= l3 <= l1:
    if (r2 < l3 and r2 + (r1 - l1) >= l3) or (r2 >= l3 and r3 < l1):
        print(1)
    elif (r3 < l1 and r3 + (r2 - l2) >= l1) or (r3 >= l1 and r2 < l3):
        print(2)
    elif (r3 < l1 and r3 + (r2 - l2) < l1) or (r2 < l3 and r2 + (r1 - l1) < l3):
        print(-1)
    elif r2 >= l3 and r3 >= l1:
        print(0)
elif l3 <= l1 <= l2:
    if (r3 < l1 and r3 + (r2 - l2) >= l1) or (r2 >= l1 and r1 < l2):
        print(2)
    elif (r1 < l2 and r1 + (r3 - l3) >= l2) or (r1 >= l2 and r3 < l1):
        print(3)
    elif (r3 < l1 and r3 + (r2 - l2) < l1) or (r1 < l2 and r1 + (r3 - l3) < l2):
        print(-1)
    elif r3 >= l1 and r1 >= l2:
        print(0)
elif l3 <= l2 <= l1:
    if (r3 < l2 and r3 + (r1 - l1) >= l2) or (r3 >= l2 and r2 < l1):
        print(1)
    elif (r2 < l1 and r2 + (r3 - l3) >= l1) or (r2 >= l1 and r3 < l2):
        print(3)
    elif (r3 < l2 and r3 + (r1 - l1) < l2) or (r2 < l1 and r2 + (r3 - l3) < l1):
        print(-1)
    elif r3 >= l2 and r2 >= l1:
        print(0)
