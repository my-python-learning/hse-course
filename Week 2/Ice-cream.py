k = int(input())

n1 = 3
n2 = 5

if k % n1 == 0 or k % n2 == 0:
    print('YES')
else:
    x1 = k % n1
    x2 = k % n2

    if x1 % n2 == 0:
        print('YES')
    elif x2 % n1 == 0:
        print('YES')
    else:
        print('NO')
