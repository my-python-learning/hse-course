N = int(input())

F, F_prev, F_prev2 = 0, 0, 0
n = 0
while n <= abs(N):
    if n == 0:
        F += 0
        F_prev2 = 0
    elif n == 1 or n == -1:
        F += 1
        F_prev = 1
    else:
        F = F_prev2 + F_prev
        F_prev2, F_prev = F_prev, F
    n += 1

print(F)
