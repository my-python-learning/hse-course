N = int(input())

d = 1
k = 0
while d < N:
    d *= 2
    k += 1
else:
    print(k)
