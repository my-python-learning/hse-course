n = int(input())

if 11 <= n < 20 or n % 10 == 5 or n % 10 == 6 or n % 10 == 7 \
        or n % 10 == 8 or n % 10 == 9 or n % 10 == 0:
    print(n, 'korov')
elif n % 10 == 1:
    print(n, 'korova')
elif n % 10 == 2 or n % 10 == 3 or n % 10 == 4:
    print(n, 'korovy')
