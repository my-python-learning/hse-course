N = int(input())
count = 0
maxNum = 0
while N != 0:
    if N > maxNum:
        maxNum = N
        count = 1
    elif N == maxNum:
        count += 1
    N = int(input())
else:
    print(count)
