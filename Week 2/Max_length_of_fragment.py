N = int(input())
greater = 0
lesser = 0
maxGreater = 0
maxLesser = 0
prev = -1
while N != 0:
    if prev == -1:
        greater = 1
        lesser = 1
    elif N > prev:
        greater += 1
        lesser = 1
    elif N < prev:
        lesser += 1
        greater = 1
    else:
        greater = 1
        lesser = 1

    prev = N

    if greater > maxGreater:
        maxGreater = greater
    elif lesser > maxLesser:
        maxLesser = lesser

    N = int(input())
else:
    print(max(maxGreater, maxLesser))
