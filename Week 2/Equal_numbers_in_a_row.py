N = int(input())
count = 0
maxCount = 0
prev = 0
while N != 0:
    if N == prev:
        count += 1
    else:
        prev = N
        count = 1

    if count > maxCount:
        maxCount = count
    N = int(input())
else:
    print(maxCount)
