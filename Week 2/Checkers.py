X1 = int(input())
Y1 = int(input())
X2 = int(input())
Y2 = int(input())

if (abs(X2 - X1) + abs(Y2 - Y1)) % 2 == 0 and 1 <= X2 <= 8 and Y1 < Y2 <= 8:
    print('YES')
else:
    print('NO')
