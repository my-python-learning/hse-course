def is_even(nums):
    for element in nums:
        if element % 2 == 0:
            return True
    return False


def is_odd(nums):
    for element in nums:
        if element % 2 == 1:
            return True
    return False


A = int(input())
B = int(input())
C = int(input())

x = [A, B, C]

if is_even(x) and is_odd(x):
    print('YES')
else:
    print('NO')
