N = int(input())
prev = N
prevprev = N
m = -1
prev_m = -1
dist = 0
count = 0
while N != 0:
    if prevprev < prev > N:
        m = count - 1

        if prev_m != -1:
            if dist == 0:
                dist = m - prev_m
            if m - prev_m < dist:
                dist = m - prev_m

        prev_m = m

    prevprev = prev
    prev = N

    count += 1
    N = int(input())
else:
    print(dist)
