N = int(input())
maxNumPrev = 0
maxNum = 0
while N != 0:
    if N > maxNum:
        maxNumPrev, maxNum = maxNum, N
    elif N > maxNumPrev:
        maxNumPrev = N
    N = int(input())
else:
    print(maxNumPrev)
