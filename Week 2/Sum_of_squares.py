N = int(input())

amount = 0
num = 1
while num <= N:
    amount += num ** 2
    num += 1

print(amount)
