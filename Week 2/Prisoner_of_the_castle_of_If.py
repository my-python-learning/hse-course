def sort_nums(a, b, c):
    if a > c:
        a, c = c, a

    if a > b:
        a, b = b, a

    if b > c:
        b, c = c, b
    return a, b, c


A, B, C = int(input()), int(input()), int(input())
D, E = int(input()), int(input())

A, B, C = sort_nums(A, B, C)

if (A * B <= D * E or A * C <= D * E or B * C <= D * E) \
        and (D >= A and E >= B) or(D >= B and E >= A):
    print('YES')
else:
    print('NO')
