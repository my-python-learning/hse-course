N = int(input())
prev = N
length = 0
while N != 0:
    if N > prev:
        length += 1
    prev = N
    N = int(input())
else:
    print(length)
