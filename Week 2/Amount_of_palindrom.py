K = int(input())

N = 1
count = 0
d = 10
while N <= K:
    n = N
    m = str()
    while n != 0:
        m += str(n % d)
        n = n // d

    if m == str(N):
        count += 1

    N += 1
else:
    print(count)
