num_list = list(map(int, input().split()))
k, C = list(map(int, input().split()))

num_list.append(num_list[-1])
for ind in range(2, len(num_list) - k):
    num_list[-ind] = num_list[-ind - 1]
else:
    num_list[k] = C
    print(' '.join(map(str, num_list)))
