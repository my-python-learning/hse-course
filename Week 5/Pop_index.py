num_list = list(map(int, input().split()))
k = int(input())

for ind in range(len(num_list) - 1):
    if ind >= k:
        num_list[ind] = num_list[ind + 1]
else:
    num_list.pop()
    print(' '.join(map(str, num_list)))
