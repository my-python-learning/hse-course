num_list = list(map(int, input().split()))
x = int(input())

pos = 0
if x > num_list[0]:
    pos = 1
elif x <= num_list[-1]:
    pos = len(num_list) + 1
else:
    for ind in range(len(num_list)):
        if num_list[ind] >= x > num_list[ind + 1]:
            pos = ind + 2

print(pos)
