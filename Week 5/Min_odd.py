num_list = list(map(int, input().split()))

max_num = num_list[0]

for elem in num_list:
    if elem > max_num:
        max_num = elem
else:
    min_odd = max_num

for elem in num_list:
    if elem % 2 != 0 and elem < min_odd:
        min_odd = elem
else:
    print(min_odd)
