num_list = list(map(int, input().split()))

for i in range(0, len(num_list) - 1, 2):
    num_list[i], num_list[i + 1] = num_list[i + 1], num_list[i]
else:
    print(' '.join(map(str, num_list)))
