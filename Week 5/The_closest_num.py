N = int(input())
num_list = list(map(int, input().split()))
x = int(input())

closest = num_list[0]
for elem in num_list:
    if abs(x - elem) < abs(x - closest):
        closest = elem
else:
    print(closest)
