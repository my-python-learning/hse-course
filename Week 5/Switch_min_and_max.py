num_list = list(map(int, input().split()))

num_max = num_list[0]
ind_max = 0
num_min = num_list[0]
ind_min = 0

for ind, elem in enumerate(num_list):
    if elem > num_max:
        num_max = elem
        ind_max = ind
    elif elem < num_min:
        num_min = elem
        ind_min = ind
else:
    num_list.pop(ind_min)
    num_list.insert(ind_min, num_max)
    num_list.pop(ind_max)
    num_list.insert(ind_max, num_min)
    print(*num_list)
