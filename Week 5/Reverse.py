num_list = list(map(int, input().split()))
reverse_list = []

for elem in num_list[::-1]:
    reverse_list.append(elem)
else:
    print(' '.join(map(str, reverse_list)))
