num_list = list(map(int, input().split()))

if len(num_list) != 0:
    count = 1
else:
    count = 0

for ind in range(1, len(num_list)):
    if num_list[ind] != num_list[ind - 1]:
        count += 1
else:
    print(count)
