def sort_list(marks_list):
    res = list()
    for i in range(100, -1, -1):
        for m in marks_list:
            if m == i:
                res.append(m)

    return res


def non_winner(marks_list):
    for m in marks_list:
        if m != marks_list[0]:
            return m


marks_9 = list()
marks_10 = list()
marks_11 = list()
with open('input.txt', 'r', encoding='utf-8') as f:
    for line in f:
        student = line.split()
        if int(student[-2]) == 9:
            marks_9.append(int(student[-1]))
        elif int(student[-2]) == 10:
            marks_10.append(int(student[-1]))
        else:
            marks_11.append(int(student[-1]))

marks_9 = sort_list(marks_9)
marks_10 = sort_list(marks_10)
marks_11 = sort_list(marks_11)

non_winner_9 = non_winner(marks_9)
non_winner_10 = non_winner(marks_10)
non_winner_11 = non_winner(marks_11)

print(non_winner_9, non_winner_10, non_winner_11)
