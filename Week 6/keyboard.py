# -*- coding: utf-8 -*-
"""
Created on Thu May  9 19:44:48 2019

@author: zapko
"""


n = int(input())
c = list(map(int, input().split()))
k = int(input())
p = list(map(int, input().split()))

press_dict = dict()
for i in p:
    if i in press_dict:
        press_dict[i] += 1
    else:
        press_dict[i] = 1

for i in range(n):
    if press_dict[i + 1] > c[i]:
        print('YES')
    else:
        print('NO')
