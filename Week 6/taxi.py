# -*- coding: utf-8 -*-
"""
Created on Tue May 21 15:26:35 2019

@author: zapko
"""


def product(list_A, list_B):
    assert len(list_A) == len(list_B)

    res = 0
    for i in range(len(list_A)):
        res += list_A[i] * list_B[i]

    return res


distance = list(map(int, input().split()))
price = list(map(int, input().split()))

distance.sort(reverse=True)
price.sort()

cost = product(distance, price)
print(cost)
