# -*- coding: utf-8 -*-
"""
Created on Thu May  9 20:47:27 2019

@author: zapko
"""


def eval_pass_score(student_list):
    if len(student_list) <= k or k == 0:
        return 0
    else:
        sorted_score_list = list()
        for i in range(120, max(score_list) + 1):
            for s in score_list:
                if s == i:
                    sorted_score_list.append(s)

        # print(sorted_score_list)
        if sorted_score_list[-k] == sorted_score_list[-k - 1]:
            for i in range(1, k):
                if sorted_score_list[-k] != sorted_score_list[-k + i]:
                    return sorted_score_list[-k + i]
            else:
                return 1
        else:
            return sorted_score_list[-k]


student_list = list()

with open('input.txt', 'r', encoding='utf-8') as f:
    for line in f:
        student_list.append(list(map(str, line.split())))

k = int(student_list[0][0])

score_list = list()
for student in student_list[1:]:
    if (int(student[-1]) >= 40 and int(student[-2]) >= 40 and
            int(student[-3]) >= 40):
        score_list.append(int(student[-1]) + int(student[-2]) +
                          int(student[-3]))

# print(score_list)

# print(sorted_score_list)

with open('output.txt', 'w', encoding='utf-8') as f:
    f.write(str(eval_pass_score(score_list)))
