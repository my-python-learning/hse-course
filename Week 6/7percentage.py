# -*- coding: utf-8 -*-
"""
Created on Tue May 21 16:28:49 2019

@author: zapko
"""

parties = dict()
flag = ''
n = 0
with open('input.txt', 'r', encoding='utf-8') as f:
    for line in f:
        line = line.replace('\n', '')
        if 'PARTIES:' in line:
            flag = 'PARTIES'
            continue
        elif 'VOTES:' in line:
            flag = 'VOTES'
            continue

        if flag == 'PARTIES':
            parties[line] = 0
        else:
            n += 1

            if line in parties:
                parties[line] += 1

percentage = {key: val / n * 100 for key, val in parties.items()
              if val / n * 100 > 7}

print(*percentage.keys(), sep='\n')
