# -*- coding: utf-8 -*-
"""
Created on Thu May  9 19:59:13 2019

@author: zapko
"""
import time

t0 = time.time()

class_9 = list()
class_10 = list()
class_11 = list()

with open('input.txt', 'r', encoding='utf-8') as f:
    for line in f:
        student = list(map(str, line.split()))
        if int(student[2]) == 9:
            class_9.append(int(student[3]))
        elif int(student[2]) == 10:
            class_10.append(int(student[3]))
        else:
            class_11.append(int(student[3]))

print(max(class_9), max(class_10), max(class_11))

t1 = time.time()

total = t1 - t0
# print(total)
