# -*- coding: utf-8 -*-
"""
Created on Thu May  9 20:15:49 2019

@author: zapko
"""

n = int(input())
student_list = list()
for i in range(n):
    student_list.append(list(map(str, input().split())))

for i in range(100, -1, -1):
    for k in student_list:
        if int(k[1]) == i:
            print(k[0])
