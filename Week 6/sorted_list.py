# -*- coding: utf-8 -*-
"""
Created on Thu May  9 18:34:44 2019

@author: zapko
"""

with open('input.txt', 'r', encoding='utf8') as f:
    text = sorted(f.readlines())
    for line in text:
        line_split = line.replace('\n', '').split(' ')
        print(line_split[0], line_split[1], line_split[3])
