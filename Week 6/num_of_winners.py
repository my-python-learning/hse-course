# -*- coding: utf-8 -*-
"""
Created on Thu May  9 20:39:46 2019

@author: zapko
"""

max_9, max_10, max_11 = 0, 0, 0
n_9, n_10, n_11 = 0, 0, 0

with open('input.txt', 'r', encoding='utf-8') as f:
    for line in f:
        student = list(map(str, line.split()))
        if int(student[2]) == 9:
            if int(student[3]) > max_9:
                max_9 = int(student[3])
                n_9 = 1
            elif int(student[3]) == max_9:
                n_9 += 1
        elif int(student[2]) == 10:
            if int(student[3]) > max_10:
                max_10 = int(student[3])
                n_10 = 1
            elif int(student[3]) == max_10:
                n_10 += 1
        else:
            if int(student[3]) > max_11:
                max_11 = int(student[3])
                n_11 = 1
            elif int(student[3]) == max_11:
                n_11 += 1

print(n_9, n_10, n_11)
