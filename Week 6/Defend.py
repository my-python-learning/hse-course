# -*- coding: utf-8 -*-
"""
Created on Tue May 21 18:43:44 2019

@author: zapko
"""

n = int(input())
dist_to_village = list(map(int, input().split()))
m = int(input())
dist_to_bombshelter = list(map(int, input().split()))

village_list = list()
bombshelter_list = list()

for i in range(n):
    village_list.append((dist_to_village[i], i + 1))

for i in range(m):
    bombshelter_list.append((dist_to_bombshelter[i], i + 1))

village_list.sort()
bombshelter_list.sort()

k = 0
res = list(None for _ in range(n))
for village in village_list:
    for i in range(k, m - 1):
        if (abs(bombshelter_list[i][0] - village[0]) <=
           abs(bombshelter_list[i + 1][0] - village[0])):
            res[village[1] - 1] = bombshelter_list[i][1]
            break
        else:
            k += 1

    if k == m - 1:
        res[village[1] - 1] = bombshelter_list[-1][1]

print(*res)
