# -*- coding: utf-8 -*-
"""
Created on Thu May  9 17:40:50 2019

@author: zapko
"""

n_9 = 0
n_10 = 0
n_11 = 0
sum_9 = 0
sum_10 = 0
sum_11 = 0
with open('input.txt', 'r', encoding='utf-8') as f:
    for line in f:
        line_split = line.split(' ')
        if int(line_split[2]) == 9:
            sum_9 += int(line_split[3])
            n_9 += 1
        elif int(line_split[2]) == 10:
            sum_10 += int(line_split[3])
            n_10 += 1
        else:
            sum_11 += int(line_split[3])
            n_11 += 1

print(sum_9 / n_9, sum_10 / n_10, sum_11 / n_11)
