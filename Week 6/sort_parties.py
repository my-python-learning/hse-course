# -*- coding: utf-8 -*-
"""
Created on Tue May 21 17:22:18 2019

@author: zapko
"""


class Party():
    name = ''
    votes = 0

    def __init__(self, name):
        self.name = name


def partyKey(party):
        return (-party.votes, party.name)


party_list = []
with open('input.txt', 'r', encoding='utf-8') as f:
    for line in f:
        line = line.replace('\n', '')
        if 'PARTIES:' in line:
            flag = 'PARTIES'
            continue
        elif 'VOTES:' in line:
            flag = 'VOTES'
            continue

        if flag == 'PARTIES':
            party = Party(line)
            party_list.append(party)
        else:
            for party in party_list:
                if line == party.name:
                    party.votes += 1

party_list.sort(key=partyKey)

for party in party_list:
    print(partyKey(party)[1])
