# -*- coding: utf-8 -*-
"""
Created on Sat Mar 23 20:27:18 2019

@author: zapko
"""


def dist(x):
    return abs(x[0] - x[1])


n = int(input())
dist_to_village = list(map(int, input().split()))

m = int(input())
dist_to_bombshelter = list(map(int, input().split()))

village_tuple = sorted([(dist_to_village[i], i) for i in range(n)])
shelter_tuple = sorted([(dist_to_bombshelter[i], i) for i in range(m)])

closest = list([None for _ in range(n)])
for village in village_tuple:
    dist_tuple = [(shelter_tuple[i][0] - village[0], shelter_tuple[i][1])
                  for i in range(m)]
    closest[village[1]] = sorted(dist_tuple, key=dist)[0][1] + 1
else:
    print(*closest)

"""
ind = 0
closest = list([None for _ in range(n)])
for village in village_tuple:
    while (ind + 1 < m and dist(village[0], bombshelter_tuple[ind][0]) >
           dist(village[0], bombshelter_tuple[ind + 1][0])):
        ind += 1
    else:
        closest[village[1]] = bombshelter_tuple[ind][1] + 1
else:
    print(*closest)
"""
