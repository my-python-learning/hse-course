# -*- coding: utf-8 -*-
"""
Created on Wed Feb 13 23:06:44 2019

@author: zapko
"""

N = int(input())

seconds = N % 60
minutes = (N // 60) % 60
hours = (N // 3600) % 24

if len(str(minutes)) == 1:
    minutes = "0" + str(minutes)

if len(str(seconds)) == 1:
    seconds = "0" + str(seconds)

print(hours, minutes, seconds, sep=":")
