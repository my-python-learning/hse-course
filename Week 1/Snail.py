# -*- coding: utf-8 -*-
"""
Created on Wed Feb 13 23:20:51 2019

@author: zapko
"""

H = int(input())
A = int(input())
B = int(input())

S = 0
day = 0

while S < H:
    if S + A >= H:
        day += 1
        break

    S += A - B
    day += 1

print(day)
