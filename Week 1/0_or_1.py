# -*- coding: utf-8 -*-
"""
Created on Wed Feb 13 22:35:16 2019

@author: zapko
"""

N = int(input())

if N == 0:
    print(1)
else:
    print(0)
