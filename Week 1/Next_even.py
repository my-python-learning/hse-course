# -*- coding: utf-8 -*-
"""
Created on Wed Feb 13 22:37:10 2019

@author: zapko
"""

N = int(input())

if bool(N % 2):
    print(N + 1)
else:
    print(N + 2)
