# -*- coding: utf-8 -*-
"""
Created on Wed Feb 13 22:27:19 2019

@author: zapko
"""

N = int(input())

print("The next number for the number", N, "is", N + 1, end='.\n')
print("The previous number for the number", N, "is", N - 1, end='.')
