N = int(input())

if N >= 10:
    print((N % 100) // 10)
else:
    print(0)
