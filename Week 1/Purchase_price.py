# -*- coding: utf-8 -*-
"""
Created on Wed Feb 13 22:19:38 2019

@author: zapko
"""

A = int(input())
B = int(input())
N = int(input())

cost = A * 100 + B

result = cost * N

print(result // 100, result % 100)
