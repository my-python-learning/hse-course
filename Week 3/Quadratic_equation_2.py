a, b, c = float(input()), float(input()), float(input())

if a != 0:
    D = b ** 2 - 4 * a * c

    if D > 0:
        x1 = (-b + D ** (1 / 2)) / (2 * a)
        x2 = (-b - D ** (1 / 2)) / (2 * a)
        print(2, x1, x2) if x1 < x2 else print(2, x2, x1)
    elif D == 0:
        x = -b / (2 * a)
        print(1, x)
    else:
        print(0)
elif a == b == 0 and c != 0:
    print(0)
elif a == b == c == 0:
    print(3)
elif a == c == 0 and b != 0:
    print(1, 0)
else:
    x = -c / b
    print(1, x)
