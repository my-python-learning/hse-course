string = input()

first = string.find('h')
last = len(string) - string[::-1].find('h') - 1

if first != last and first != -1:
    new_string = string[:first + 1] + string[first + 1:last].replace('h', 'H')\
                 + string[last:]
    print(new_string)
