p = int(input())
rub, cop = int(input()), int(input())
k = int(input())

deposit = rub + cop / 100
i = 1
while i <= k:
    in_year = deposit * (p + 100) / 100
    rub = int(in_year)
    cop = int((in_year - rub) * 100 + 10 ** -6)
    deposit = rub + cop / 100
    i += 1
else:
    print(rub, cop)
