x = int(input())

summ = 0
summ2 = 0
n = 0
while x != 0:
    summ2 += x ** 2
    summ += x
    n += 1
    x = int(input())
else:
    s = summ / n
    sigma = ((summ2 - 2 * summ * s + s ** 2 * n) / (n - 1)) ** (1 / 2)
    print(sigma)
