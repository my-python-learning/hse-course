p = int(input())
x, y = int(input()), int(input())

deposit = x + y / 100
in_year = deposit * (p + 100) / 100
rub = int(in_year)
cop = int((in_year - rub) * 100 + 10 ** -6)

print(rub, cop)
