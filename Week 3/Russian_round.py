import math

x = float(input())

print(math.floor(x)) if x < int(x) + 0.5 else print(math.ceil(x))
